<?php

session_start();
/**
 * APLICAÇÃO: WebHookMeAviseBot
 * AUTOR: Nataniel Pereira dos Santos
 * DESCRIÇÃO: Código responsável pelas respostas do bot do telegram tanto para as 
 * requisiões diretas do bot quando para as que forem feitas pelo aplicativo da TV Digital
 * DATA: 08/07/2017 
 */

// 488024519:AAHHk9iMFiD_i3UxBs2_AGZSWt9pOkqLT_0 (bot smartvhelp)

define('BOT_TOKEN', '488024519:AAHHk9iMFiD_i3UxBs2_AGZSWt9pOkqLT_0');
define('API_URL', 'https://api.telegram.org/bot'.BOT_TOKEN.'/');
function processMessage($message) {
  // processa a mensagem recebida
  $message_id = $message['message_id']; 
  $chat_id = $message['chat']['id'];
  if (isset($message['text'])) {
    
    $text = $message['text'];//texto recebido na mensagem 
    $text = preg_replace("/[óòôõö]/", "o", $text); //retira acento
    $text = strtolower($text); // passa para minúscula
    
    if (strpos($text, "/start") === 0) {
		//envia a mensagem ao usuário
      sendMessage("sendMessage", array('chat_id' => $chat_id, "text" => 'Olá, '. $message['from']['first_name'].
		'! Eu sou um bot que alerta quando alguém precisar de sua ajuda.'));
      sendMessage("sendMessage", array('chat_id' => $chat_id, "text" => 'Digite "codigo" para receber seu identificador. Ele deve ser informado para o usuário do aplicativo na TV.'));
    } else if ($text === "codigo") {
      sendMessage("sendMessage", array('chat_id' => $chat_id, "text" => $chat_id));
    } else {
      sendMessage("sendMessage", array('chat_id' => $chat_id, "text" => 'Desculpe, mas não entendi essa mensagem. :('));
    }
  } else {
    sendMessage("sendMessage", array('chat_id' => $chat_id, "text" => 'Desculpe, mas só compreendo mensagens em texto'));
  }
}
function sendMessage($method, $parameters) {
  $options = array(
  'http' => array(
    'method'  => 'POST',
    'content' => json_encode($parameters),
    'header'=>  "Content-Type: application/json\r\n" .
                "Accept: application/json\r\n"
    )
);
$context  = stream_context_create( $options );
file_get_contents(API_URL.$method, false, $context );
}
/*Com o webhook setado, não precisamos mais obter as mensagens através do método getUpdates.Em vez disso, 
* como o este arquivo será chamado automaticamente quando o bot receber uma mensagem, utilizamos "php://input"
* para obter o conteúdo da última mensagem enviada ao bot. 
*/
$update_response = file_get_contents("php://input");
$update = json_decode($update_response, true);


// RESPOSTA PARA REQUISIÇÕES AO BOT
if (isset($update["message"])) {
  processMessage($update["message"]);

// RESPOSTA CASO SEJA ENVIADO UM ALERTA
}else{
    
  //$nome = $_POST["nome"];
  $id = $_GET["id"];

try {
 /* $pdo = new PDO('mysql:host=localhost;dbname=usuario', 'root', '2039327');
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   
  $stmt = $pdo->prepare('SELECT codigo FROM usuarios WHERE id = :idUsuario');
  $stmt->execute(array(
    ':idUsuario' => $id,
  ));
  
  $cod = $stmt->fetchAll(PDO::FETCH_ASSOC);
  
  echo $cod[0]['codigo'];*/
    $cod = $_SESSION[$codigo];
    
} catch(PDOException $e) {
  echo 'Error: ' . $e->getMessage();
}   
    
    
    
 sendMessage("sendMessage", array('chat_id' => $cod, "text" => 'Mensagem de teste enviada pelo aplicativo Smart Help TV!'));
 
 echo ('Seu alerta foi enviado com sucesso !');
}
?>