<?php

define('BOT_TOKEN', '488024519:AAHHk9iMFiD_i3UxBs2_AGZSWt9pOkqLT_0');
define('API_URL', 'https://api.telegram.org/bot'.BOT_TOKEN.'/');

require_once 'lib/nusoap.php';

$server = new soap_server;

$server->configureWSDL('server.codigo', 'urn:server.codigo');
$server->wsdl->schemaTargetNamespace = 'urn:server.codigo';
$server->register('codigo',
        array('cod' => 'xsd:string',
                'txt' => 'xsd:string'),
        array('return' => 'xsd:string'),
        'urn:server.codigo',
        'urn:server.codigo#codigo',
        'rpc',
        'encoded',
        'Recupera o codigo informado pelo telespectador no aplicacao digital'
);

function sendMessage($method, $parameters) {
  $options = array(
  'http' => array(
    'method'  => 'POST',
    'content' => json_encode($parameters),
    'header'=>  "Content-Type: application/json\r\n" .
                "Accept: application/json\r\n"
    )
);
$context  = stream_context_create( $options );
file_get_contents(API_URL.$method, false, $context );
}

function codigo($cod, $txt) {

    $sendMessage = sendMessage("sendMessage", array('chat_id' => $cod, "text" => $txt));

    if (isset($sendMessage)){
        $msg = "Alerta enviada para ".$cod;
    }else{
        $msg = 'Mensagem enviada para '.$cod;
    }

    return $msg;
}

//$server->service($HTTP_RAW_POST_DATA); Essa chamada está obsoleta para o PHP7

$server->service(file_get_contents('php://input'));
?>
